package com.gfe.ganblik.intivetest.models;

public class Results {
    private Picture picture;

    private Id id;

    private String phone;

    private String email;

    private Location location;

    private String registered;

    private String cell;

    private String dob;

    private Name name;

    private String gender;

    private String nat;

    private Login login;

    public Picture getPicture ()
    {
        return picture;
    }

    public Id getId ()
    {
        return id;
    }

    public String getPhone ()
    {
        return phone;
    }

    public String getEmail ()
    {
        return email;
    }

    public Location getLocation ()
    {
        return location;
    }

    public String getRegistered ()
    {
        return registered;
    }

    public String getCell ()
    {
        return cell;
    }

    public String getDob ()
    {
        return dob;
    }

    public Name getName ()
    {
        return name;
    }

    public String getGender ()
    {
        return gender;
    }

    public String getNat ()
    {
        return nat;
    }

    public Login getLogin ()
    {
        return login;
    }

}
