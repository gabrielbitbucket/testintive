package com.gfe.ganblik.intivetest.models;

import java.util.ArrayList;

public class Response {
    private ArrayList<Results> results;

    private Info info;

    public ArrayList<Results> getResults ()
    {
        return results;
    }

    public Info getInfo ()
    {
        return info;
    }

}
