package com.gfe.ganblik.intivetest.models;

public class Picture {
    private String thumbnail;

    private String medium;

    private String large;

    public String getThumbnail ()
    {
        return thumbnail;
    }

    public String getMedium ()
    {
        return medium;
    }


    public String getLarge ()
    {
        return large;
    }
}
