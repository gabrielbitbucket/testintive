package com.gfe.ganblik.intivetest.models;

public class Location {
    private String street;

    private String state;

    private String postcode;

    private String city;

    public String getStreet ()
    {
        return street;
    }

    public String getState ()
    {
        return state;
    }

    public String getPostcode ()
    {
        return postcode;
    }

    public String getCity ()
    {
        return city;
    }

}
