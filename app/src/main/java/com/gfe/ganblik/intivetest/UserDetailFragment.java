package com.gfe.ganblik.intivetest;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gfe.ganblik.intivetest.models.Results;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class UserDetailFragment extends Fragment {

    public static final String ARG_ITEM_ID = "item_id";

    private Results mUser;

    public UserDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID)) {

            mUser = UserListActivity.itemListObjects.get(getArguments().getInt(ARG_ITEM_ID));

          }
    }

    protected void callOpenDial(String number){
        try {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:"+number));
            startActivity(intent);
        }
        catch (ActivityNotFoundException e){
            e.printStackTrace();
        }
    }

    protected void openEmailTo(String emailTo){
        try {
            Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

            /* Fill it with Data */
            emailIntent.setType("plain/text");
            emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{emailTo});

            /* Send it off to the Activity-Chooser */
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
        }
        catch (ActivityNotFoundException e){
            e.printStackTrace();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.user_detail, container, false);

        if (mUser != null) {
            ((TextView) rootView.findViewById(R.id.name_detail)).setText(mUser.getName().getTitle() + ". " + mUser.getName().getFirst() + " "+ mUser.getName().getLast());
            ((TextView) rootView.findViewById(R.id.email)).setText(mUser.getEmail());
            ((TextView) rootView.findViewById(R.id.phone)).setText(mUser.getPhone());
            ((TextView) rootView.findViewById(R.id.cellphone)).setText(mUser.getCell());
            ((TextView) rootView.findViewById(R.id.user_name)).setText(mUser.getLogin().getUsername());

            ( rootView.findViewById(R.id.phone_section)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callOpenDial(mUser.getPhone());
                }
            });

            ( rootView.findViewById(R.id.cellphone)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callOpenDial(mUser.getCell());
                }
            });

            ( rootView.findViewById(R.id.email_section)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openEmailTo(mUser.getEmail());
                }
            });

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date d = dateFormat.parse((mUser.getDob()).substring(0,10));
                if (d != null) {
                    LinearLayout dboSection = rootView.findViewById(R.id.dob_section);
                    TextView dobTextView = ( rootView.findViewById(R.id.dob));
                    dboSection.setVisibility(View.VISIBLE);
                    dobTextView.setText(dateFormat.format(d));
                }

                d = dateFormat.parse((mUser.getRegistered()).substring(0,10));
                if (d != null) {
                    LinearLayout regSection = rootView.findViewById(R.id.reg_section);
                    TextView dobTextView = ( rootView.findViewById(R.id.registered));
                    regSection.setVisibility(View.VISIBLE);
                    dobTextView.setText(dateFormat.format(d));
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return rootView;
    }
}
