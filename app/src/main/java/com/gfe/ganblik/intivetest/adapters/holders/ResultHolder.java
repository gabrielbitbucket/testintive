package com.gfe.ganblik.intivetest.adapters.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.gfe.ganblik.intivetest.R;


public class ResultHolder extends RecyclerView.ViewHolder {

    public TextView userName, userCellphone;
    public ImageView userImage;


    public ResultHolder(View itemView) {
        super(itemView);
        userName = itemView.findViewById(R.id.user_name);
        userImage =  itemView.findViewById(R.id.user_image);
        userCellphone = itemView.findViewById(R.id.user_cellphone);

    }
}