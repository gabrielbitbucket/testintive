package com.gfe.ganblik.intivetest.models;

public class Name {
    private String title;

    private String last;

    private String first;

    public String getTitle ()
    {
        return title;
    }

    public String getLast ()
    {
        return last;
    }

    public String getFirst ()
    {
        return first;
    }
}
