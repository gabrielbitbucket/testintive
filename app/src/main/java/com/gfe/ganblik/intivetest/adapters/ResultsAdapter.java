package com.gfe.ganblik.intivetest.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gfe.ganblik.intivetest.R;
import com.gfe.ganblik.intivetest.adapters.holders.ResultHolder;
import com.gfe.ganblik.intivetest.interfaces.OnBottomReachedListener;
import com.gfe.ganblik.intivetest.models.Results;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ResultsAdapter extends RecyclerView.Adapter<ResultHolder> {

        private List<Results> itemList;
        private Context context;
        private OnBottomReachedListener onBottomReachedListener;
        private OnItemClickListener listener;

        public interface OnItemClickListener {
            void onItemClick(Results user, int position);
        }

        public ResultsAdapter(Context context, List<Results> itemList, OnItemClickListener listener ) {
            this.itemList = itemList;
            this.context = context;
            this.listener = listener;
        }

        @Override
        public ResultHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_user_detail, null);
            ResultHolder rcv = new ResultHolder(layoutView);
            return rcv;
        }

        @Override
        public void onBindViewHolder(ResultHolder holder, final int position) {
            final Results user = itemList.get(position);
            Picasso.get().load((itemList.get(position).getPicture().getMedium())).placeholder(R.drawable.ic_launcher_foreground).into(holder.userImage);
            holder.userName.setText(user.getName().getTitle() + ". "+ user.getName().getFirst() + " "+ user.getName().getLast() );
            holder.userCellphone.setText(user.getCell());
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(user, position);
                }
            });

            if (position == itemList.size() - 1) onBottomReachedListener.onBottomReached(position);

        }

        @Override
        public int getItemCount() {
            return this.itemList.size();
        }

        public void setOnBottomReachedListener(OnBottomReachedListener onBottomReachedListener){
            this.onBottomReachedListener = onBottomReachedListener;
        }
}
