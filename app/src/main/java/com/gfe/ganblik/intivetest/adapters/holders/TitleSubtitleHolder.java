package com.gfe.ganblik.intivetest.adapters.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.gfe.ganblik.intivetest.R;


public class TitleSubtitleHolder extends RecyclerView.ViewHolder {
    public TextView title, subtitle;


    public TitleSubtitleHolder(View itemView) {
        super(itemView);
        title = itemView.findViewById(R.id.title);
       // subtitle =  itemView.findViewById(R.id.subtitle);
    }
}