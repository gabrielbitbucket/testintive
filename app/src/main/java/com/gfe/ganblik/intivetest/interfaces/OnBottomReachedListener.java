package com.gfe.ganblik.intivetest.interfaces;

public interface OnBottomReachedListener {

    void onBottomReached(int position);

}