package com.gfe.ganblik.intivetest.models;

public class Info {
    private String results;

    private String page;

    private String seed;

    private String version;

    public String getResults ()
    {
        return results;
    }

    public String getPage ()
    {
        return page;
    }

    public String getSeed ()
    {
        return seed;
    }

    public String getVersion ()
    {
        return version;
    }
}
