package com.gfe.ganblik.intivetest;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ImageView;

import com.gfe.ganblik.intivetest.models.Results;
import com.squareup.picasso.Picasso;

import static com.gfe.ganblik.intivetest.UserDetailFragment.ARG_ITEM_ID;

public class UserDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_detail);
        Toolbar toolbar = findViewById(R.id.detail_toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");

        ImageView imagePerson = findViewById(R.id.image_person);
        Results mUser = UserListActivity.itemListObjects.get(getIntent().getIntExtra(ARG_ITEM_ID,0));
        Picasso.get().load(mUser.getPicture().getLarge()).placeholder(R.drawable.ic_launcher_foreground).into(imagePerson);

        if (savedInstanceState == null) {
            // Create the detail fragment and add it to the activity
            // using a fragment transaction.
            Bundle arguments = new Bundle();
            arguments.putInt(ARG_ITEM_ID,
                    getIntent().getIntExtra(ARG_ITEM_ID, 0));
            UserDetailFragment fragment = new UserDetailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.user_detail_container, fragment)
                    .commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            this.finish();
            //NavUtils.navigateUpTo(this, new Intent(this, UserListActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
