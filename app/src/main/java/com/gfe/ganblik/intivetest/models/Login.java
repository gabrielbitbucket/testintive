package com.gfe.ganblik.intivetest.models;

public class Login {
    private String username;

    private String sha256;

    private String md5;

    private String sha1;

    private String password;

    private String salt;

    public String getUsername ()
    {
        return username;
    }

    public String getSha256 ()
    {
        return sha256;
    }

    public String getMd5 ()
    {
        return md5;
    }

    public String getSha1 ()
    {
        return sha1;
    }

    public String getPassword ()
    {
        return password;
    }

    public String getSalt ()
    {
        return salt;
    }
}
