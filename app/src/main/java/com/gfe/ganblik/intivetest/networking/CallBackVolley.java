package com.gfe.ganblik.intivetest.networking;


import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

public interface CallBackVolley {
    void onSuccess(JsonObject result) throws JsonParseException;
    void onError(String result) throws Exception;
}
