package com.gfe.ganblik.intivetest;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.gfe.ganblik.intivetest.adapters.ResultsAdapter;
import com.gfe.ganblik.intivetest.interfaces.OnBottomReachedListener;
import com.gfe.ganblik.intivetest.models.Results;
import com.gfe.ganblik.intivetest.networking.CallBackVolley;
import com.gfe.ganblik.intivetest.networking.CustomRequest;
import com.gfe.ganblik.intivetest.networking.NetworkController;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class UserListActivity extends AppCompatActivity {

    private boolean mTwoPane;
    private Gson gson;
    private int page = 1;
    private boolean haveMoreResults = true;

    private RecyclerView recyclerView;
    public static ArrayList<Results> itemListObjects = new ArrayList<>();
    private StaggeredGridLayoutManager staggeredGridLayoutManager;
    private ResultsAdapter rcAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        GsonBuilder gsonBuilder = new GsonBuilder();
        gson = gsonBuilder.create();

        if (findViewById(R.id.user_detail_container) != null) {

            mTwoPane = true;
        }

        recyclerView = findViewById(R.id.user_list);


        staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, 1);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);

        rcAdapter = new ResultsAdapter(UserListActivity.this ,itemListObjects , new ResultsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Results user, int position) {

                Intent productPageIntent = new Intent(UserListActivity.this, UserDetailActivity.class);
                productPageIntent.putExtra(UserDetailFragment.ARG_ITEM_ID, position);
                startActivity(productPageIntent);

            }
        });
        recyclerView.setAdapter(rcAdapter);

        rcAdapter.setOnBottomReachedListener(new OnBottomReachedListener() {
            @Override
            public void onBottomReached(int position) {
                if (haveMoreResults) {
                    haveMoreResults = false;
                    search();//get more users
                }
            }
        });


        search();
    }

    protected void search(){

        makeRequest("https://randomuser.me/api?results=50&page="+page, new CallBackVolley() {
            @Override
            public void onSuccess(JsonObject result) throws JsonParseException {

                com.gfe.ganblik.intivetest.models.Response resultSearch = gson.fromJson( result, com.gfe.ganblik.intivetest.models.Response.class);
                page++;
                itemListObjects.addAll(resultSearch.getResults());
                rcAdapter.notifyDataSetChanged();
                haveMoreResults = true;

            }

            @Override
            public void onError(String result) throws Exception {
                Toast.makeText(getApplicationContext(), getString(R.string.app_name),
                        Toast.LENGTH_LONG).show();
                //haveMoreResults = false;
                haveMoreResults = true;
            }
        });
    }

    public void makeRequest(final String url, final CallBackVolley callback) {

        CustomRequest rq = new CustomRequest(Request.Method.GET,
                url, null,
                new Response.Listener<JsonObject>() {
                    @Override
                    public void onResponse(JsonObject response) {
                        try {
                            callback.onSuccess(response);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.v("Response", error.toString());
                        String err = null;
                        if (error instanceof com.android.volley.NoConnectionError){
                            err = "No internet Access!";
                        }
                        try {
                            if(err != "null") {
                                callback.onError(err);
                            }
                            else {
                                callback.onError(error.toString());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }

        };
        rq.setPriority(Request.Priority.HIGH);
        NetworkController.getInstance(getApplicationContext()).addToRequestQueue(rq);

    }
}
